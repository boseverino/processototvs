<h3>Hi 👋, I'm Bruno de Oliveira Severino</h3>
<h5>A passionate frontend developer from Brazil</h3>

- 🔭 I’m currently working on **Design System Projects**

<h5 align="left">Connect with me:</h3>
<p align="left">
<a href="https://twitter.com/severasp" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="severasp" height="30" width="40" /></a>
<a href="https://linkedin.com/in/bruno.severino" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="bruno.severino" height="30" width="40" /></a>
<a href="https://instagram.com/boseverino" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="boseverino" height="30" width="40" /></a>
</p>

<br />

<h3>Processo TOTVS</h3>

<h4>Install</h4>
<h5>Clone and Install Dependencies</h5>
<code>
  git clone git@gitlab.com:boseverino/processototvs.git
  cd processototvs
  npm install // or yarn add
</code>

<h4>Build</h4>
<h5>To show the compoents ('button'/'select') just run the storybook</h5>
<code>
npm run storybook
</code>


<h5>Authors</h5>
- [@bruno_severino](https://gitlab.com/bruno_severino)
