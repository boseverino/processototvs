import { moduleMetadata, type Meta, type StoryObj, argsToTemplate } from '@storybook/angular';
import { CustomBtnComponent } from './custom-btn.component';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faTrashCan, faRefresh, faHome } from '@fortawesome/free-solid-svg-icons';
import { APP_INITIALIZER } from '@angular/core';

const meta: Meta<CustomBtnComponent> = {
  title: 'Example/Button',
  component: CustomBtnComponent,
  tags: ['autodocs'],
  render: (args: CustomBtnComponent) => ({
    props: {
      ...args,
      iconText: 'faRefresh'
    },
  }),
  decorators: [
    moduleMetadata({
      imports: [FontAwesomeModule],
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (iconLibrary: FaIconLibrary) => async() => {
            // Add any icons needed here:
            iconLibrary.addIcons(faRefresh);
            iconLibrary.addIcons(faTrashCan);
            iconLibrary.addIcons(faHome);
          },
          // When using a factory provider you need to explicitly specify its dependencies.
          deps: [FaIconLibrary],
          multi: true,
        },
      ],
    }),
  ],
};

export default meta;
type Story = StoryObj<CustomBtnComponent>;

export const Primary: Story = {
  args: {
    type: 'button',
    kind: 'primary',
    label: 'Primary',
    size: 'normal',
    disabled: false,
    danger: false,
  },
};
export const Secondary: Story = {
    args: {
        type: 'button',
        kind: 'secondary',
        label: 'Secondary',
        size: 'normal',
        disabled: false,
        danger: false,
    },
};
export const Tertiary: Story = {
    args: {
        type: 'button',
        kind: 'tertiary',
        label: 'Tertiary',
        size: 'normal',
        disabled: false,
        danger: false,
    },
};
export const DangerPrimary: Story = {
    args: {
        type: 'button',
        kind: 'primary',
        label: 'Danger',
        size: 'normal',
        disabled: false,
        danger: true,
    },
};
export const DangerSecondary: Story = {
    args: {
        type: 'button',
        kind: 'secondary',
        label: 'Danger',
        size: 'normal',
        disabled: false,
        danger: true,
    },
};
export const Disabled: Story = {
    args: {
        type: 'button',
        kind: 'primary',
        label: 'Disabled',
        size: 'normal',
        disabled: true,
        danger: true,

    },
};

export const icon= () => ({
  template: `
      <custom-btn [icon]='icon'>
      </custom-btn>

      <ng-template #icon>
        <fa-icon [icon]="trash"></fa-icon>
      </ng-template>
  `,
  // Provide the icons as props:
  props: {
    trash: faTrashCan,
  },
});

export const iconText = () => ({
  template: `
      <custom-btn [icon]='icon' label='label'>
      </custom-btn>

      <ng-template #icon>
        <fa-icon [icon]="refresh"></fa-icon>
      </ng-template>
  `,
  // Provide the icons as props:
  props: {
    refresh: faRefresh,
  },
});