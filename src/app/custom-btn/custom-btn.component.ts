import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

@Component({
  selector: 'custom-btn',
  imports: [CommonModule],
  styleUrls: ['./custom-btn.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<button
    type={{type}}
    [ngClass]="classList"
    (click)="disabled ? $event.preventDefault() : onClick.emit($event)"
    tabindex="0"
    [ariaDisabled]="disabled"
  >
    <ng-container *ngIf="icon" [ngTemplateOutlet]="icon"></ng-container>
    <span  *ngIf="label !== undefined" class="title">{{label}}</span>
  </button>`,
})
export class CustomBtnComponent implements OnInit{
  @Input() label: string | undefined;
  @Input() kind: 'primary' | 'secondary' | 'tertiary' = 'primary';
  @Input() size: 'normal' | 'large' = 'normal';
  @Input() disabled: boolean = false;
  @Input() danger: boolean = false;
  @Input() icon: TemplateRef<any> | undefined;
  
  @Input() type: 'button' | 'submit' | 'reset' = 'button';

  @Output() onClick = new EventEmitter<Event>();

  constructor() { }
  ngOnInit(): void {}

  public get classList(): string[] {
    return [
      'custom-btn', 
      `${this.kind ? this.kind : ''}`, 
      `${this.size ? this.size : ''}`, 
      `${this.disabled ? 'disabled' : ''}`, 
      `${this.danger ? 'danger' : ''}`, 
      `${this.icon ? 'withIcon' : ''}`, 
    ]; 
  }
}
