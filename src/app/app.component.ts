import { Component } from '@angular/core';
import { faRefresh, faTrashCan } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  customBtn = `<custom-btn label='teste' kind='primary' disabled='false' danger='false'></custom-btn>`
  customBtnClicked = 0;
  customBtnEvent = '';
  customBtnClick(e: any) {
    this.customBtnClicked++;
    this.customBtnEvent = e.currentTarget.outerHTML;
  }
  faTrashCan = faTrashCan;
  faRefresh = faRefresh;


}
