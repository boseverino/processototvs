import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PoModule } from '@po-ui/ng-components';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CustomBtnComponent } from './custom-btn/custom-btn.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CustomSelectComponent } from './custom-select/custom-select.component';
@NgModule({
    declarations: [
        AppComponent,
    ],
    providers: [],
    bootstrap: [AppComponent],
    imports: [
      FontAwesomeModule,
      CustomBtnComponent,
      CustomSelectComponent,
      BrowserModule,
      AppRoutingModule,
      PoModule,
      HttpClientModule,
      RouterModule.forRoot([]),
    ]
})
export class AppModule { }
