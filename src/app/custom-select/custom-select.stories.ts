import { type Meta, type StoryObj } from '@storybook/angular';
import { CustomSelectComponent } from './custom-select.component';

const meta: Meta<CustomSelectComponent> = {
  title: 'Example/Select',
  component: CustomSelectComponent,
  tags: ['autodocs'],
  render: (args: CustomSelectComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;
type Story = StoryObj<CustomSelectComponent>;

export const SelectExample: Story = {
  args: {
    label: 'Label',
    disabled: false,
    required: true,
    placeholder: 'Placeholder...',
    helper: 'helper text',
    options: [
      {value: 'option1', label: 'option1'},
      {value: 'option2', label: 'option2'},
      {value: 'option3', label: 'option3'},
    ],
  },
};