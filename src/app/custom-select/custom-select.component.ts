import { CommonModule } from '@angular/common';
import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { ElementRef } from 'react';


interface options {
    value: string,
    label: string,
}

@Component({
  selector: 'custom-select',
  imports: [CommonModule],
  styleUrls: ['./custom-select.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<label
    [ngClass]="classList"
  > 
    <span  *ngIf="label !== undefined" class="title">{{label}}</span>
    <select 
        #select
        (click)="onClick.emit($event)"
        (change)="changeSelect($event)"
        [required]="required"
        [disabled]="disabled"
    >
        <option *ngIf="placeholder" disabled hidden selected class="placeholder">{{placeholder}}</option>
        <ng-container *ngIf="options">
            <option *ngFor="let option of options" [value]="option.value">{{option.label}}</option>
        </ng-container>
    </select>
    <span  *ngIf="helper !== undefined" class="helper">{{helper}}</span>
  </label>`,
})
export class CustomSelectComponent implements AfterViewInit {
    @ViewChild('select') select: any;

    @Input() label: string | undefined;
    @Input() disabled: boolean = false;
    @Input() required: boolean = false;
    @Input() placeholder: string | undefined;
    @Input() helper: string | undefined;
    @Input() options: options[] | undefined;
    
    @Output() onClick = new EventEmitter<Event>();
    @Output() onChange = new EventEmitter<Event>();

    ngAfterViewInit(): void {
        this.placeholder ? this.select?.nativeElement.classList.add('placeholder') : '';
    }

    changeSelect(event: any): void {
        this.placeholder ? this.select?.nativeElement.classList.remove('placeholder') : '';
        this.onChange.emit(event);
    }

    public get classList(): string[] {
        return [
        'custom-select', 
        `${this.disabled ? 'disabled' : ''}`, 
        ]; 
    }
}
